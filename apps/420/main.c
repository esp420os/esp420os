#include "stdint.h"
#include <stdio.h>

typedef struct {
    char    name[255];
    size_t  addr;
} symbol;

int _420() {
    void* (*m)(int s) = 0x40086d6c;
    int32_t (*read_flash)(size_t s, void* d, size_t l) = 0x400d526c;
    int (*ps)(char* p) = 0x400d5dc8;
    m(420);

    char* msg = m(64);
    msg[0] = 'a';
    msg[1] = 'b';
    msg[2] = 'c';
    msg[3] = '\0';

    ps(msg);

    size_t idx = 0x150000;
    symbol sym;
    char d = 'd';
    for(;;) {
        //read_flash(idx, &sym, sizeof(symbol));
        //if(sym.addr == 420)
            //break;
        //ps(sym.name);
        //if(sym.name[0] == d) {
            //void (*doit)() = sym.addr;
            //doit();
            //break;
        //}
        idx += sizeof(symbol);
        void (*doit)() = 0x400d38f8;
        doit();
        break;
    }

    return 420;
}