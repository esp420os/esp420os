#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_partition.h"

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>


static int
os_get_free_memory(lua_State* l) {
    lua_pushinteger(l, esp_get_free_heap_size());
    return 1;
}

luaL_Reg os_reg[] = {
    {"free_mem", os_get_free_memory},
    {NULL, NULL}
};

lua_State*
os_new_state() {
    lua_State *vm = luaL_newstate();
    luaL_openlibs(vm);

    luaL_newlib(vm, os_reg);
    lua_setglobal(vm, "esp");

    if(luaL_dostring(vm, "print('free mem: '..esp.free_mem())")) {
        printf("lua error: %s\n", lua_tostring(vm, 1));
    }

    return vm;
}

void
doit()
{
    printf("DOIT\n");
}

typedef struct {
    char    name[255];
    size_t  addr;
} symbol;

void
write_symbol(symbol sym, size_t dest)
{
    spi_flash_write(dest, &sym, sizeof(symbol));
    printf("wrote symbol: %s = 0x%x : %x\n", sym.name, sym.addr, dest);
}

void 
write_symbol_table(size_t start) 
{
    symbol sym;

    //puts(char *)
    strcpy(sym.name, "puts");
    sym.addr = (size_t)&puts;
    write_symbol(sym, start);
    start += sizeof(symbol);

    strcpy(sym.name, "doit!");
    sym.addr = (size_t)&doit;
    write_symbol(sym, start);
    start += sizeof(symbol);

    //write footer
    sym.addr = 420;
    strcpy(sym.name, "END");
    write_symbol(sym, start);
}

void
app_main(void)
{
    printf("spi_flash_read is @ %p\n", &spi_flash_read);
    printf("malloc is @ %p\n", &malloc);
    esp_partition_t *part, *symtab;
    spi_flash_mmap_handle_t mmap_h;
    void *out;

    part = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, 143, "os_app");
    symtab = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, 145, "symbol_table");

    write_symbol_table(symtab->address);

    ESP_ERROR_CHECK( esp_partition_mmap(part, 0x140, 0x4000, SPI_FLASH_MMAP_INST, &out, &mmap_h) );

    printf("INST Start: %p\n", out);

    int (*func)() = out;
    for(;;) {
        printf("_420() = %d\n", func());
        printf("mem: %d\n", esp_get_free_heap_size());
        vTaskDelay(100/portTICK_RATE_MS);
    }
}
